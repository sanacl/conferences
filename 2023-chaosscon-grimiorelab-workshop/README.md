# Finding Your Newcomers

 - Title: Finding Your Newcomers
 - Conference: CHAOSScon 2023
 - Date: Feb 3rd 2023
 - Slide deck: available [here](https://speakerdeck.com/canasdiaz/grimoirelab-workshop-find-your-newcomers)

## Retention rate over time

![](images/timeline-diff.png)

```
.es(index=git, timefield=demography_min_date,q='author_bot:false', metric=cardinality:author_uuid).bars().label('Joining').color(#ffe082),

.es(index=git, timefield=demography_max_date,q='author_bot:false AND demography_max_date:<now-6M', metric=cardinality:author_uuid).bars(stack=false).label('Leaving').color(#81d4fa).multiply(-1),

.es(index=git, timefield=demography_max_date,q='author_bot:false AND demography_max_date:>=now-6M AND demography_max_date:<now-3M', metric=cardinality:author_uuid).bars(stack=false).label('Maybe Leaving').color(#9fa8da).multiply(-1),


.es(index=git, timefield=demography_min_date,q='author_bot:false AND demography_min_date:<now-6M', metric=cardinality:author_uuid).subtract(.es(index=git, timefield=demography_max_date,q='author_bot:false AND demography_max_date:<now-6M', metric=cardinality:author_uuid)).mvavg(window=20w, position=left).label('Joining - Leaving (mvg avg)')
```

## Size of the development community over time

![](images/timeline-cusum.png)

```

.es(index=git, timefield=demography_min_date,q='author_bot:false', metric=cardinality:author_uuid).bars().label('Joining').color(#ffe082),

.es(index=git, timefield=demography_max_date,q='author_bot:false AND demography_max_date:<now-6M', metric=cardinality:author_uuid).bars(stack=false).label('Leaving').color(#81d4fa).multiply(-1),

.es(index=git, timefield=demography_max_date,q='author_bot:false AND demography_max_date:>=now-6M AND demography_max_date:<now-3M', metric=cardinality:author_uuid).bars(stack=false).label('Maybe Leaving').color(#9fa8da).multiply(-1),

.es(index=git, timefield=demography_min_date,q='author_bot:false', metric=cardinality:author_uuid).subtract(.es(index=git, timefield=demography_max_date,q='author_bot:false AND demography_max_date:<now-3M', metric=cardinality:author_uuid)).label('Joining - Leaving (mvg avg)').cusum().trim('end',4)
```

## Newcomers: who are they? where do they start contributing?

![](images/whoarethey.png)

Left table:
  - metric: min
  - field: `demography_min_date`
  - split by rows:
    - terms: `author_name`
    - order by: Min demgraphy_min_date
    - size: 100
  - split by rows:
    - terms: `author_uuid`
    - order by: Min demgraphy_min_date
    - size: 100

Vertical bar chart:
  - metric: unique count
  - field: `author_uuid`
  - split series:
    - terms: `tz`
    - order by: alphabetical
    - size: 24

Table at the bottom right:
  - metric: unique count
  - field: `hash`
  - split by rows:
    - terms: `repo_name`
    - order by: unique count of hash
    - size: 100



## Activity in Git commits by experience

![](images/experience.png)

Scripted field called `author_experience` (integer):

```
def FirstDate =  doc['demography_min_date'].value.getMillis();
def SecondDate = doc['grimoire_creation_date'].value.getMillis();

def diff = SecondDate - FirstDate;

def diff_days = diff / (1000*60*60*24);

def experience = diff_days / 365;

return experience
```

Vertical bar chart:
 - Y-axis:
   - metric: unique count
   - field: `hash`
 - X-axis:
   - Aggregation: Date Histogram
   - field: `grimoire_creation_date`
   - minimum interval: Month
 - split series:
   - sub aggregation: filters
   - filter 1: `author_experience <= 1`
   - filter 2: `author_experience <= 2 and author_experience >1`
   - filter 3: `author_experience > 2`
